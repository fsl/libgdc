/* GDCHART 0.10.0dev  GDCHART.H  2 Nov 2000 */
/* Copyright Bruce Verderaime 1998, 1999, 2000 */

#ifndef _GDCHART_H
#define _GDCHART_H

#include "gdc.h"

#define MAX_NOTE_LEN		19
#define GDC_INTERP_VALUE	(GDC_NOVALUE/2.0)	/* must set GDC_interpolations */
#define GDC_INTERP			((GDC_interpolations=TRUE),GDC_INTERP_VALUE)


typedef enum {
			 GDC_LINE,
			 GDC_AREA,
			 GDC_BAR,
			 GDC_FLOATINGBAR,
			 GDC_HILOCLOSE,
			 GDC_COMBO_LINE_BAR,			/* aka, VOL[ume] */
			 GDC_COMBO_HLC_BAR,
			 GDC_COMBO_LINE_AREA,
			 GDC_COMBO_LINE_LINE,
			 GDC_COMBO_HLC_AREA,
			 GDC_3DHILOCLOSE,
			 GDC_3DCOMBO_LINE_BAR,
			 GDC_3DCOMBO_LINE_AREA,
			 GDC_3DCOMBO_LINE_LINE,
			 GDC_3DCOMBO_HLC_BAR,
			 GDC_3DCOMBO_HLC_AREA,
			 GDC_3DBAR,
			 GDC_3DFLOATINGBAR,
			 GDC_3DAREA,
			 GDC_3DLINE
			 } GDC_CHART_T;

typedef enum {
			 GDC_STACK_DEPTH,				/* "behind" (even non-3D) */
			 GDC_STACK_SUM,
			 GDC_STACK_BESIDE,
			 GDC_STACK_LAYER
			 } GDC_STACK_T;					/* applies only to num_lines > 1 */

typedef enum {
			 GDC_HLC_DIAMOND         = 1,
			 GDC_HLC_CLOSE_CONNECTED = 2,	/* can't be used w/ CONNECTING */
			 GDC_HLC_CONNECTING      = 4,	/* can't be used w/ CLOSE_CONNECTED */
			 GDC_HLC_I_CAP           = 8
			 } GDC_HLC_STYLE_T;				/* can be OR'd */

											/* only 1 annotation allowed */
typedef struct
			{
			float			point;			/* 0 <= point < num_points */
			unsigned long	color;
			char			note[MAX_NOTE_LEN+1];	/* NLs ok here */
			} GDC_ANNOTATION_T;

typedef enum {
			 GDC_SCATTER_TRIANGLE_DOWN,
			 GDC_SCATTER_TRIANGLE_UP,
			 GDC_SCATTER_CIRCLE
			 } GDC_SCATTER_IND_T;
typedef struct
			{
			float				point;		/* 0 <= point < num_points */
			float				val;
			unsigned short		width;		/* % (1-100) */
			unsigned long		color;
			GDC_SCATTER_IND_T	ind;
			} GDC_SCATTER_T;

typedef enum {
			 GDC_TICK_LABELS = -2,			/* only at labels */
			 GDC_TICK_POINTS = -1,			/* each num_points */
			 GDC_TICK_NONE	 = 0
			 /* > GDC_TICK_NONE */			/* points & inter-point */
			 } GDC_TICK_T;

typedef enum {								/* backward compatible w/ FALSE/TRUE */
			 GDC_BORDER_NONE = 0,
			 GDC_BORDER_ALL  = 1,			/* should be GDC_BORDER_Y|Y2|X|TOP */
			 GDC_BORDER_X    = 2,
			 GDC_BORDER_Y    = 4,
			 GDC_BORDER_Y2   = 8,
			 GDC_BORDER_TOP  = 16
			 } GDC_BORDER_T;

/****************************************************/
/********** USER CHART OPTIONS w/ defaults **********/
/****************************************************/
extern char              *GDC_ytitle;
extern char              *GDC_xtitle;
extern char              *GDC_ytitle2; /* ostesibly: volume label */
extern char              *GDC_title;   /* NLs ok here */
extern enum GDC_font_size GDC_title_size;
extern enum GDC_font_size GDC_ytitle_size;
extern enum GDC_font_size GDC_xtitle_size;
extern enum GDC_font_size GDC_yaxisfont_size;
extern enum GDC_font_size GDC_xaxisfont_size;
extern double             GDC_xaxis_angle; /* 0,90. FT: 0-90 */
#ifdef HAVE_LIBFREETYPE
extern char              *GDC_title_font;
extern char              *GDC_ytitle_font;
extern char              *GDC_xtitle_font;
extern char              *GDC_yaxis_font;
extern char              *GDC_xaxis_font;
extern double             GDC_title_ptsize;
extern double             GDC_ytitle_ptsize;
extern double             GDC_xtitle_ptsize;
extern double             GDC_yaxis_ptsize;
extern double             GDC_xaxis_ptsize;
#endif
extern char              *GDC_ylabel_fmt;     /* printf fmt'ing, e.g.: "%.2f" */
extern char              *GDC_ylabel2_fmt;    /* default: "%.0f" future: fractions */
extern char              *GDC_xlabel_ctl;     /* num_points[] TRUE,FALSE */
extern short              GDC_xlabel_spacing; /* pixels  SHRT_MAX means force all */
extern char               GDC_ylabel_density; /* % */
extern char               GDC_interpolations; /* GDC_INTERP_VALUE in data */
extern float              GDC_requested_ymin;
extern float              GDC_requested_ymax;
extern float              GDC_requested_yinterval;
extern char               GDC_0Shelf; /* if applicable */
extern GDC_TICK_T         GDC_grid;
extern GDC_TICK_T         GDC_ticks;
extern char               GDC_xaxis;
extern char               GDC_yaxis;
extern char               GDC_yaxis2;
extern char               GDC_yval_style;
extern GDC_STACK_T        GDC_stack_type;
extern float              GDC_3d_depth; /* % img size */
extern unsigned char      GDC_3d_angle; /* 1-89 */
extern unsigned char      GDC_bar_width; /* % (1-100) */
extern GDC_HLC_STYLE_T    GDC_HLC_style;
extern unsigned char      GDC_HLC_cap_width; /* % (1-100) */
extern GDC_ANNOTATION_T  *GDC_annotation;
extern enum GDC_font_size GDC_annotation_font_size;
#ifdef HAVE_LIBFREETYPE
extern char              *GDC_annotation_font;
extern double             GDC_annotation_ptsize;
#endif
extern int                GDC_num_scatter_pts;
extern GDC_SCATTER_T     *GDC_scatter;
extern char               GDC_thumbnail;
extern char              *GDC_thumblabel;
extern float              GDC_thumbval;
extern GDC_BORDER_T       GDC_border;
extern unsigned long      GDC_BGColor; /* black */
extern unsigned long      GDC_GridColor; /* gray */
extern unsigned long      GDC_LineColor;
extern unsigned long      GDC_PlotColor;
extern unsigned long      GDC_VolColor; /* lgtblue1 */
extern unsigned long      GDC_TitleColor; /* "opposite" of BG */
extern unsigned long      GDC_XTitleColor;
extern unsigned long      GDC_YTitleColor;
extern unsigned long      GDC_YTitle2Color;
extern unsigned long      GDC_XLabelColor;
extern unsigned long      GDC_YLabelColor;
extern unsigned long      GDC_YLabel2Color; /* supercedes VolColor    ulong_color[num_points] */
extern unsigned long      *GDC_ExtVolColor; /* supercedes LineColor    ulong_color[num_sets] */
extern unsigned long      *GDC_SetColor; /* supercedes SetColor    ulong_color[num_sets][num_points] */
extern unsigned long      *GDC_ExtColor;
extern char                GDC_transparent_bg;
extern char               *GDC_BGImage;
/* legends?  separate img? */
/* auto-size fonts, based on image size? */

/* ----- following options are for expert users only ----- */
/* for alignment of multiple charts */
/* USE WITH CAUTION! */
extern char GDC_hard_size;
extern int  GDC_hard_xorig;      /* in/out */
extern int  GDC_hard_graphwidth; /* in/out */
extern int  GDC_hard_yorig;      /* in/out */
extern int  GDC_hard_grapheight; /* in/out */

int GDC_out_graph( short		IMGWIDTH,		/* no check for a image that's too small to fit */
				   short		IMGHEIGHT,
				   FILE			*img_fptr,		/* open file pointer (iamge out)      stdout ok */
				   GDC_CHART_T	type,
				   int			num_points,     /* points along x axis (even iterval)         */
												/*	all arrays dependant on this              */
				   char			*xlbl[],		/* array of xlabels                           */
				   int			num_sets,
				   float		*data,			/* based on num_sets X num_points             */
				   float		*combo_data );	/* only used on COMBO chart types             */
/* ----- backward compatible var arg interface ----- */
int out_graph( short		imgwidth,
			   short		imgheight,
			   FILE			*img_fptr,		/* open file pointer (image out) */
			   GDC_CHART_T	type,
			   int			num_points,		/* points along x axis (even iterval) */
			   char			*xlbl[],
			   int			num_sets,
							... );
/* expected params (...) for each chart type:
GDC_LINE
GDC_BAR
GDC_3DBAR
GDC_3DAREA
GDC_AREA			float	vals[], ...
												multiple sets make sense for rest?
GDC_HILOCLOSE		float	high[],
					float	low[],
					float	close[]

GDC_COMBO_LINE_BAR
GDC_COMBO_LINE_AREA	float	val[],
					float   vol[]

GDC_COMBO_HLC_BAR
GDC_COMBO_HLC_AREA	float   high[],
                    float   low[],
                    float   close[],
					float   vol[]

*/

/* Notes:
	GDC_thumbnail = TRUE
	is equivelent to:	GDC_grid = FALSE
						GDC_xaxis = FALSE
						GDC_yaxis = FALSE
*/

#endif /*!_GDCHART_H*/
