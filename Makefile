include ${FSLCONFDIR}/default.mk

PROJNAME   = libgdc
LIBS       = -lgd -lpng
SOFILES    = libgdc.so
TESTXFILES = gdc_samp1 gdc_samp2 gdc_pie_samp

ifeq "$(CC)" "gcc"
   GCCVERSIONGTEQ4 := $(shell expr `gcc -dumpversion | awk -F. '{ printf "%i.%i", $$1, $$2 }'` = 4.0)

   ifeq "$(GCCVERSIONGTEQ4)" "1"
      # MUST compile without optimisation as gcc-4.0.1 has a loop unrolling bug (or something)
      USRINCFLAGS += O0
   endif
endif

OBJS = gifencode.o price_conv.o gdc.o gdc_pie.o gdchart.o array_alloc.o

all: libgdc.so

test: $(TESTXFILES)

libgdc.so: ${OBJS}
	$(CC) $(CFLAGS) -shared -o $@ $^ $(LDFLAGS)

gdc_samp1: gdc_samp1.o
	$(CC)  ${CFLAGS} -o $@ $^ -lgdc $(LDFLAGS)

gdc_samp2: gdc_samp2.o
	$(CC)  ${CFLAGS} -o $@ $^ -lgdc ${LDFLAGS}

gdc_pie_samp: gdc_pie_samp.o
	$(CC)  ${CFLAGS} -o $@ $^ -lgdc ${LDFLAGS}
