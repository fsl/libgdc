/* GDCHART 0.10.0dev  GDCHART.H  2 Nov 2000 */
/* Copyright Bruce Verderaime 1998, 1999, 2000, 2001 */

#ifndef _GDCPIE_H
#define _GDCPIE_H

#include "gdc.h"

typedef enum {
             GDC_3DPIE,
             GDC_2DPIE
             } GDCPIE_TYPE;

typedef enum {
             GDCPIE_PCT_NONE,
             GDCPIE_PCT_ABOVE,		/* relative to label, if any */
             GDCPIE_PCT_BELOW,
             GDCPIE_PCT_RIGHT,
             GDCPIE_PCT_LEFT
             } GDCPIE_PCT_TYPE;


/**************************************************/
/**** USER DEFINABLE PIE OPTIONS  w/ defaults *****/
/**************************************************/
extern unsigned long      GDCPIE_BGColor; /* black */
extern unsigned long      GDCPIE_PlotColor; /* gray */
extern unsigned long      GDCPIE_LineColor;
extern unsigned long      GDCPIE_EdgeColor; /* edging on/off */

extern char               GDCPIE_other_threshold;
extern unsigned short     GDCPIE_3d_angle; /* 0-360 */
extern unsigned short     GDCPIE_3d_depth; /* % image width */
extern unsigned short     GDCPIE_perspective; /* % view */
extern char              *GDCPIE_title; /* NLs ok here */
extern enum GDC_font_size GDCPIE_title_size;
extern enum GDC_font_size GDCPIE_label_size;
#ifdef HAVE_LIBFREETYPE
extern char              *GDCPIE_title_font;
extern char              *GDCPIE_label_font;
extern double             GDCPIE_title_ptsize;
extern double             GDCPIE_label_ptsize;
#endif
extern int                GDCPIE_label_dist; /* 1+GDC_fontc[GDCPIE_label_size].h/2 */
extern unsigned char      GDCPIE_label_line; /* from label to slice */

extern int               *GDCPIE_explode;     /* [num_points] */
                                              /* [num_points] supercedes GDCPIE_PlotColor */
extern unsigned long     *GDCPIE_Color;
extern unsigned char     *GDCPIE_missing;     /* TRUE/FALSE */

extern GDCPIE_PCT_TYPE    GDCPIE_percent_labels;
extern const char        *GDCPIE_percent_fmt; /* printf fmt'ing */

void GDC_out_pie( short			width,
				  short			height,
				  FILE*,						/* open file pointer, can be stdout */
				  GDCPIE_TYPE,
				  int			num_points,
				  char			*labels[],		/* slice labels */
				  float			data[] );

#endif /*!_GDCPIE_H*/
